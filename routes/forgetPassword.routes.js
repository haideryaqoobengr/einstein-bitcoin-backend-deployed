var crypto = require("crypto");
var bcrypt = require("bcryptjs");
const Sequelize = require('sequelize');
var aws     = require('aws-sdk');
aws.config.loadFromPath(__dirname + '/config.json');
// Instantiate SES.
var ses = new aws.SES();
const Op = Sequelize.Op;
const db = require("../models");
const ResetToken = db.resetToken;
const User = db.user;

module.exports = app => {
    var router = require("express").Router();

    router.post("/", async function (req, res, next) {
        //ensure that you have a user with this email
        var email = await User.findOne({ where: { email: req.body.email } });
        if (email == null) {
            /**
             * we don't want to tell attackers that an
             * email doesn't exist, because that will let
             * them use this form to find ones that do
             * exist.
             **/
            return res.json({ status: 'ok' });
        }
        /**
         * Expire any tokens that were previously
         * set for this user. That prevents old tokens
         * from being used.
         **/
        await ResetToken.update({
            used: 1
        },
            {
                where: {
                    email: req.body.email
                }
            });

        //Create a random reset token
        var token = crypto.randomBytes(64).toString('base64');

        //token expires after one hour
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 1);

        //insert token data into DB
        await ResetToken.create({
            email: req.body.email,
            expiration: expireDate,
            token: token,
            used: 0
        });
        console.log("token: ", token);
        console.log("email: ", req.body.email);
        
        var email = "support@einsteinbitcoin.com"
        var ses_mail = "From: 'Einstein Bitcoin' <" + email + ">\n";
        ses_mail = ses_mail + "To: " + req.body.email + "\n";
        ses_mail = ses_mail + "Subject: FORGET PASSWORD\n";
        ses_mail = ses_mail + "MIME-Version: 1.0\n";
        ses_mail = ses_mail + "Content-Type: multipart/mixed; boundary=\"NextPart\"\n\n";
        ses_mail = ses_mail + "--NextPart\n";
        ses_mail = ses_mail + "Content-Type: text/html; charset=us-ascii\n\n";
        ses_mail = ses_mail + 'To reset your password, please click the link.\n\nhttp://' + 'haideryaqoob.com' + '/Resetpass?token=' + encodeURIComponent(token) + '&email=' + req.body.email
        
        var params = {
            RawMessage: { Data: Buffer.from(ses_mail) },
            Destinations: [ req.body.email ],
            Source: "'EINSTEIN BITCOIN' <" + email + ">'"
        };
        
        ses.sendRawEmail(params, function(err, data) {
            if(err) {
                res.send(err);
            } 
            else {
                res.send(data);
            }           
        });
    });

    router.post("/changePassword", async function (req, res, next) {
      // clear all expired tokens
        await ResetToken.destroy({
            where: {
                expiration: { [Op.lt]: Sequelize.fn('CURDATE') },
            }
        });
        console.log("email is: ", req.body.email);
        console.log("token is: ", req.body.token);
        //find the token
        var record = await ResetToken.findOne({
            where: {
                email: req.body.email,
                expiration: { [Op.gt]: Sequelize.fn('CURDATE') },
                token: req.body.token,
                used: 0
            }
        });

        if (record == null) {
            return res.json({ status: 'error', message: 'Token not found. Please try the reset password process again.' });
        }

        //compare passwords
        if (req.body.password1 !== req.body.password2) {
            return res.json({ status: 'error', message: 'Passwords do not match. Please try again.' });
        }

        var upd = await ResetToken.update({
            used: 1
        },
            {
                where: {
                    email: req.body.email
                }
            });

        var newSalt = crypto.randomBytes(64).toString('hex');
        console.log("newSalt: ", newSalt);

        await User.update({
            password: bcrypt.hashSync(req.body.password1, 8),
        },
            {
                where: {
                    email: req.body.email
                }
            });

        return res.json({ status: 'ok', message: 'Password reset. Please login with your new password.' }); res.render('user/reset-password', {
            showForm: true,
            record: record
        });
    });

    app.use('/user/forgetPassword', router);

};
